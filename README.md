**Summary**

Arch User Repository (AUR) package for the https://screen.so `screen-desktop` screen sharing application. 

Visit https://screen.so/ for more information.

**Development Notes**  

* Test PKGBUILD changes > `makepkg --install --syncdeps`  
* Generate .SRCINFO (mandatory) `makepkg --printsrcinfo > .SRCINFO`
